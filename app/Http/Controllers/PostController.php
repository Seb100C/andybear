<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

class PostController extends Controller
{
    public function show($slug)
    {
      $post=Post::where('slug',$slug)->firstOrFail();
      //$post= \DB::table('posts')->where('slug', $slug)->first();
      //dd($post);

      return view('post', [
        'post'=>$post
      ]);

    }
}
