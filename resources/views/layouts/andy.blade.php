<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <!-- <link href="{{ asset('css/carousel.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/andy.css') }}" rel="stylesheet">
    <link href="{{ asset('css/carousel.css') }}" rel="stylesheet">
  </head>
<body>


  <nav class="navbar navbar-expand-md fixed-top bg-light">
  <a class="navbar-brand" href="{{$url=route('home')}}">
 <img src="{{ asset('image/logo.png')}}" alt="Andy Bear Early Learning Center" style="width:350px;">
  </a>
  <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"> -->
  <button class="navbar-toggler collapsed andy_bg_yellow" data-target="#navbarCollapse" data-toggle="collapse">
     <span class="line andy_bg_green"></span>
     <span class="line andy_bg_blue"></span>
     <span class="line andy_bg_red"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto pill-nav">
      <li class="nav-item">
        <a class="nav-link {{ Request::is('/')?'active':''}}" href="{{$url=route('home')}}">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Facility</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Programs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Ressources</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('contact-us')?'active':''}}" href="{{$url=route('contactus')}}">Contact us</a>
      </li>
    </ul>
</div>
    <div class="mr-sm-2"><h4 class="fun"><span class="andy_green">Infant Care.</span> <span class="andy_blue">Daycare.</span> <span class="andy_red">Preschool.</span> <span class="andy_yellow">After-School.</span></h4></div>

  </nav>


  <!-- //  <div id="app"> -->
        @yield('content')
    <!-- </div> -->
    <footer class="container">
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>Andy Bear Workshop, LLC. License #50178 &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a>
    <br>11500 Wiles Rd
    <br>Coral Springs, FL 33076</p>

    </footer>

</body>
</html>
